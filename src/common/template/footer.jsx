import React from 'react'

export default props => (
    <footer className='main-footer'> 
        <strong> 
            Copyright &copy; 2017
            <a href='http://zup.com.br' target='_blank'> Zup</a>.
        </strong>
    </footer>
)